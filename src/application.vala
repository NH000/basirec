/*
 * Copyright (C) 2021-2022 Nikola Hadžić
 *
 * This file is part of Basirec.
 *
 * Basirec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Basirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Basirec.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Basirec
{
    class Application : Gtk.Application
    {
        // Command-line options.
        private bool no_notifications = false;
#if BASIREC_AUDIO_SYSTEM_PULSE
        private string? pulseaudio_server = null;
#endif

        // App configuration.
        private Gee.HashMap<string, GLib.Variant> config;

        // Detected recording devices.
        private Gee.HashMap<string, string> devices_cameras;
        private Gee.HashMap<string, string> devices_microphones;

        // Mapping between recording devices combo box item IDs and device keys.
        private Gee.ArrayList<string?> devices_cameras_from_combo_box_items;
        private Gee.ArrayList<string?> devices_microphones_from_combo_box_items;

        // Program resources.
        private string res_main_window;
        private string res_preview_window;
        private string res_resource_busy_dialog;
        private string res_recording_in_progress_quit_dialog;
        private string res_settings_dialog;

        // Signal connection IDs.
        private ulong sig_chooser_camera_changed;

        // Media pipeline.
        private Pipeline pipeline;

        // Main window.
        private Gtk.ApplicationWindow main_window;
        private Gtk.Button button_record;
        private Gtk.Button button_pause;
        private Gtk.Button button_picture;
        private Gtk.ComboBoxText chooser_camera;
        private Gtk.ComboBoxText chooser_microphone;
        private Gtk.Button button_settings;

        // Preview window.
        private Gtk.ApplicationWindow preview_window;

        private Application(string? application_id)
        {
            Object(application_id: application_id, flags: GLib.ApplicationFlags.NON_UNIQUE);

            /* COMMAND-LINE SETUP */

            add_main_option_entries({{"version", 'v', GLib.OptionFlags.NONE, GLib.OptionArg.NONE, null, GLib._("Display program version and exit"), null},
                                     {"license", 'l', GLib.OptionFlags.NONE, GLib.OptionArg.NONE, null, GLib._("Display program license and exit"), null},
                                     {"no-notifications", '\0', GLib.OptionFlags.NONE, GLib.OptionArg.NONE, &no_notifications, GLib._("Do not display notifications"), null},
#if BASIREC_AUDIO_SYSTEM_PULSE
                                     {"pulseaudio-server", '\0', GLib.OptionFlags.NONE, GLib.OptionArg.STRING, ref pulseaudio_server, GLib._("PulseAudio server to connect to"), GLib._("SERVER")},
#endif
                                     {null, '\0', GLib.OptionFlags.NONE, GLib.OptionArg.NONE, null, null, null}});
        }

        public override int handle_local_options(GLib.VariantDict options)
        {
            if (options.contains("version"))
            {
                stdout.printf("%s: %s\n", GLib._("Version"), Symbols.VERSION);
                return 0;
            }

            if (options.contains("license"))
            {
                stdout.printf("%s: %s\n", GLib._("License"), Symbols.LICENSE);
                return 0;
            }

#if BASIREC_AUDIO_SYSTEM_PULSE
            if (pulseaudio_server == "")
                pulseaudio_server = null;
#endif

            return -1;
        }

        public override void startup()
        {
            base.startup();

            // Make custom widgets known to the type system.
            typeof(FileChooserButton).ensure();

            // Read app configuration from the configuration file.
            config = new Gee.HashMap<string, GLib.Variant>();
            read_config();
        }

        public override void shutdown()
        {
            // Save program configuration to the configuration file.
            write_config();

            base.shutdown();
        }

        // Reads app configuration from the configuration file.
        private void read_config()
        {
            var key_file = new GLib.KeyFile();

            try
            {
                key_file.load_from_file(GLib.Path.build_filename(GLib.Environment.get_user_config_dir(), GLib.Filename.from_utf8(Symbols.EXEC_NAME + ".ini", -1, null, null)), GLib.KeyFileFlags.KEEP_COMMENTS | GLib.KeyFileFlags.KEEP_TRANSLATIONS);
            }
            catch (GLib.ConvertError error)
            {
                stderr.printf(GLib._("Error reading the configuration file: "));
                stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
            }
            catch (GLib.KeyFileError error)
            {
                stderr.printf(GLib._("Error reading the configuration file: "));
                stderr.printf(GLib._("Invalid key file structure: %s.\n"), error.message);
            }
            catch (GLib.FileError error)
            {
                stderr.printf(GLib._("Error reading the configuration file: "));
                stderr.printf(GLib._("File error: %s.\n"), error.message);
            }

            try
            {
                if (!GLib.Path.is_absolute(key_file.get_string("saving", "recordings-save-dir")))
                    throw new GLib.KeyFileError.INVALID_VALUE(null);
            }
            catch (GLib.KeyFileError error)
            {
                try
                {
                    key_file.set_string("saving", "recordings-save-dir", GLib.Filename.to_utf8(GLib.Path.build_filename(GLib.Environment.get_home_dir(), GLib.Filename.from_utf8("Recordings", -1, null, null)), -1, null, null));
                }
                catch (GLib.ConvertError error)
                {
                    stderr.printf(GLib._("Error setting the default configuration value: "));
                    stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
                }
            }
            finally
            {
                try { config["saving-recordings-save-dir"] = new GLib.Variant.string(key_file.get_string("saving", "recordings-save-dir")); } catch (GLib.KeyFileError error) { }
            }
            try
            {
                if (!GLib.Path.is_absolute(key_file.get_string("saving", "pictures-save-dir")))
                    throw new GLib.KeyFileError.INVALID_VALUE(null);
            }
            catch (GLib.KeyFileError error)
            {
                try
                {
                    key_file.set_string("saving", "pictures-save-dir", GLib.Filename.to_utf8(GLib.Path.build_filename(GLib.Environment.get_home_dir(), GLib.Filename.from_utf8("Pictures", -1, null, null), GLib.Filename.from_utf8("Photos", -1, null, null)), -1, null, null));
                }
                catch (GLib.ConvertError error)
                {
                    stderr.printf(GLib._("Error setting the default configuration value: "));
                    stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
                }
            }
            finally
            {
                try { config["saving-pictures-save-dir"] = new GLib.Variant.string(key_file.get_string("saving", "pictures-save-dir")); } catch (GLib.KeyFileError error) { }
            }
            try
            {
                if (key_file.get_string("saving", "output-filename-format").length == 0)
                    throw new GLib.KeyFileError.INVALID_VALUE(null);
            }
            catch (GLib.KeyFileError error)
            {
                key_file.set_string("saving", "output-filename-format", Symbols.EXEC_NAME + " - %c");
            }
            finally
            {
                try { config["saving-output-filename-format"] = new GLib.Variant.string(key_file.get_string("saving", "output-filename-format")); } catch (GLib.KeyFileError error) { }
            }
        }

        // Writes app configuration to the configuration file.
        private void write_config()
        {
            var key_file = new GLib.KeyFile();

            key_file.set_string("saving", "recordings-save-dir", config["saving-recordings-save-dir"].get_string());
            key_file.set_string("saving", "pictures-save-dir", config["saving-pictures-save-dir"].get_string());
            key_file.set_string("saving", "output-filename-format", config["saving-output-filename-format"].get_string());

            try
            {
                key_file.save_to_file(GLib.Path.build_filename(GLib.Environment.get_user_config_dir(), GLib.Filename.from_utf8(Symbols.EXEC_NAME + ".ini", -1, null, null)));
            }
            catch (GLib.ConvertError error)
            {
                stderr.printf(GLib._("Error writing the configuration file: "));
                stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
            }
            catch (GLib.FileError error)
            {
                stderr.printf(GLib._("Error writing the configuration file: "));
                stderr.printf(GLib._("File error: %s.\n"), error.message);
            }
        }

        public override void activate()
        {
            try
            {
                // Detect available recording devices.
                devices_cameras = Query.find_cameras();
                devices_microphones = Query.find_microphones(
#if BASIREC_AUDIO_SYSTEM_PULSE
                    pulseaudio_server
#endif
                );

                // Retrieve program resources.
                var resource = get_resource();
                res_main_window = (string) resource.lookup_data("/org/gtk/" + Symbols.EXEC_NAME + "/glade/main_window.glade", GLib.ResourceLookupFlags.NONE).get_data();
                res_preview_window = (string) resource.lookup_data("/org/gtk/" + Symbols.EXEC_NAME + "/glade/preview_window.glade", GLib.ResourceLookupFlags.NONE).get_data();
                res_recording_in_progress_quit_dialog = (string) resource.lookup_data("/org/gtk/" + Symbols.EXEC_NAME + "/glade/recording_in_progress_quit_dialog.glade", GLib.ResourceLookupFlags.NONE).get_data();
                res_settings_dialog = (string) resource.lookup_data("/org/gtk/" + Symbols.EXEC_NAME + "/glade/settings_dialog.glade", GLib.ResourceLookupFlags.NONE).get_data();
                res_resource_busy_dialog = (string) resource.lookup_data("/org/gtk/" + Symbols.EXEC_NAME + "/glade/resource_busy_dialog.glade", GLib.ResourceLookupFlags.NONE).get_data();

                // Create media pipeline.
                pipeline = new Pipeline();

                // Create and show application windows.
                do_main_window();
                do_preview_window();
            }
            catch (PipelineError error)
            {
                stderr.printf(GLib._("Error activating the application: "));
                stderr.printf(GLib._("Pipeline error: %s.\n"), error.message);
            }
            catch (GLib.Error error)
            {
                stderr.printf(GLib._("Error activating the application: "));
                stderr.printf(GLib._("Generic error: %s.\n"), error.message);
            }
        }

        // Creates and shows the main window.
        private void do_main_window()
        {
            var builder_main_window = new Gtk.Builder.from_string(res_main_window, -1);
            main_window = (Gtk.ApplicationWindow) builder_main_window.get_object("main-window");
            button_record = (Gtk.Button) builder_main_window.get_object("button-record");
            button_pause = (Gtk.Button) builder_main_window.get_object("button-pause");
            button_picture = (Gtk.Button) builder_main_window.get_object("button-picture");
            chooser_camera = (Gtk.ComboBoxText) builder_main_window.get_object("chooser-camera");
            chooser_microphone = (Gtk.ComboBoxText) builder_main_window.get_object("chooser-microphone");
            button_settings = (Gtk.Button) builder_main_window.get_object("button-settings");

#if BASIREC_GFX_BACKEND_X11
            ((Gtk.Widget) main_window).realize.connect(() => {
                if (((Gdk.X11.Display) main_window.get_display()).get_screen().supports_net_wm_hint("_NET_WM_STATE_ABOVE"))
                {
                    var display = new X.Display(main_window.get_display().get_name());
                    var atom_state_above = display.intern_atom("_NET_WM_STATE_ABOVE", false);

                    display.change_property(((Gdk.X11.Surface) main_window.get_surface()).get_xid(), display.intern_atom("_NET_WM_STATE", false), X.XA_ATOM, 32, X.PropMode.Append, (uchar[]) &atom_state_above, 1);
                }
            });
#endif
            main_window.close_request.connect(window_close);

            button_record.clicked.connect(action_button_clicked);
            button_pause.clicked.connect(action_button_clicked);
            button_picture.clicked.connect(action_button_clicked);

            devices_cameras_from_combo_box_items = new Gee.ArrayList<string?>();
            devices_cameras_from_combo_box_items.add(null);
            foreach (var camera in devices_cameras)
            {
                chooser_camera.append_text(camera.value);
                devices_cameras_from_combo_box_items.add(camera.key);
            }

            devices_microphones_from_combo_box_items = new Gee.ArrayList<string?>();
            devices_microphones_from_combo_box_items.add(null);
            foreach (var microphone in devices_microphones)
            {
                chooser_microphone.append_text(microphone.value);
                devices_microphones_from_combo_box_items.add(microphone.key);
            }

            sig_chooser_camera_changed = chooser_camera.changed.connect(device_changed);
            chooser_microphone.changed.connect(device_changed);

            button_settings.clicked.connect(show_settings_dialog);

            main_window.icon_name = Symbols.EXEC_NAME;
            main_window.title = Symbols.TITLE;
            main_window.application = this;
            main_window.show();
        }

        // Creates and shows the camera preview window.
        private void do_preview_window()
        {
            var builder_preview_window = new Gtk.Builder.from_string(res_preview_window, -1);
            preview_window = (Gtk.ApplicationWindow) builder_preview_window.get_object("preview-window");

            var camera_preview = pipeline.get_camera_preview();
            preview_window.set_child(camera_preview);

            preview_window.close_request.connect(window_close);

            preview_window.icon_name = Symbols.EXEC_NAME;
            preview_window.application = this;
            preview_window.show();
        }

        private void device_changed(Gtk.ComboBox chooser)
        {
            var camera_active = chooser_camera.get_active() != 0;
            var microphone_active = chooser_microphone.get_active() != 0;

            button_record.sensitive = camera_active || microphone_active;
            button_picture.sensitive = camera_active;
            button_record.tooltip_text = (camera_active || microphone_active) ? GLib._("Start recording") : null;
            button_picture.tooltip_text = camera_active ? GLib._("Take a picture") : null;

            if (chooser == chooser_camera)
            {
                var camera = devices_cameras_from_combo_box_items[chooser_camera.get_active()];

                if (camera == null)
                    pipeline.set_state(Gst.State.NULL);
                else
                {
                    try
                    {
                        pipeline.set_video_source(camera);
                    }
                    catch (PipelineError error)
                    {
                        GLib.SignalHandler.block(chooser_camera, sig_chooser_camera_changed);
                        chooser_camera.set_active(0);
                        GLib.SignalHandler.unblock(chooser_camera, sig_chooser_camera_changed);

                        var builder_resource_busy_dialog = new Gtk.Builder.from_string(res_resource_busy_dialog, -1);
                        var resource_busy_dialog = (Gtk.MessageDialog) builder_resource_busy_dialog.get_object("resource-busy-dialog");

                        resource_busy_dialog.transient_for = main_window;
                        resource_busy_dialog.text = GLib._("This camera (%s) is already used by another process.").printf(camera);

                        resource_busy_dialog.response.connect(() => {
                            resource_busy_dialog.destroy();
                        });

                        var icon = new Gtk.Image.from_icon_name("dialog-error-symbolic");
                        icon.icon_size = Gtk.IconSize.LARGE;
                        resource_busy_dialog.get_content_area().prepend(icon);

                        resource_busy_dialog.show();
                    }
                }
            }
        }

        private void action_button_clicked(Gtk.Button button)
        {
            if (button == button_record)
            {
                if (pipeline.is_recording)
                    pipeline.stop_recording();
                else
                    pipeline.start_recording(null, "");

                button_record.icon_name = pipeline.is_recording ? "media-playback-stop-symbolic" : "media-record-symbolic";
                button_record.tooltip_text = pipeline.is_recording ? GLib._("Stop recording") : GLib._("Start recording");
                button_pause.sensitive = pipeline.is_recording;
                if (!pipeline.is_recording) button_pause.icon_name = "media-playback-pause-symbolic";
                button_pause.tooltip_text = pipeline.is_recording ? GLib._("Pause recording") : null;
                button_picture.sensitive = !pipeline.is_recording && chooser_camera.get_active() != 0;
                button_picture.tooltip_text = !pipeline.is_recording && chooser_camera.get_active() != 0 ? GLib._("Take a picture") : null;
                chooser_camera.sensitive = !pipeline.is_recording;
                chooser_microphone.sensitive = !pipeline.is_recording;
                button_settings.sensitive = !pipeline.is_recording;
            }
            else if (button == button_pause)
            {
                if (pipeline.is_recording_paused)
                    pipeline.resume_recording();
                else
                    pipeline.pause_recording();

                button_pause.icon_name = pipeline.is_recording_paused ? "media-playback-start-symbolic" : "media-playback-pause-symbolic";
                button_pause.tooltip_text = pipeline.is_recording_paused ? GLib._("Resume recording") : GLib._("Pause recording");
            }
            else if (button == button_picture)
                pipeline.take_a_picture("");
        }

        // Displays settings dialog to the user.
        private void show_settings_dialog()
        {
            var builder_settings_dialog = new Gtk.Builder.from_string(res_settings_dialog, -1);
            var settings_dialog = (Gtk.Dialog) builder_settings_dialog.get_object("settings-dialog");
            var saving_recordings_save_dir_button = (FileChooserButton) builder_settings_dialog.get_object("saving-recordings-save-dir-button");
            var saving_pictures_save_dir_button = (FileChooserButton) builder_settings_dialog.get_object("saving-pictures-save-dir-button");
            var saving_output_filename_format_entry = (Gtk.Entry) builder_settings_dialog.get_object("saving-output-filename-format-entry");

            settings_dialog.transient_for = main_window;

            saving_recordings_save_dir_button.label = config["saving-recordings-save-dir"].get_string();
            saving_recordings_save_dir_button.dialog_transient_for = settings_dialog;

            saving_pictures_save_dir_button.label = config["saving-pictures-save-dir"].get_string();
            saving_pictures_save_dir_button.dialog_transient_for = settings_dialog;

            saving_output_filename_format_entry.buffer.set_text((uint8[]) config["saving-output-filename-format"].get_string());
            saving_output_filename_format_entry.changed.connect(() => {
                settings_dialog.get_widget_for_response(Gtk.ResponseType.OK).set_sensitive(validate_settings_dialog_state(builder_settings_dialog));
            });

            settings_dialog.response.connect((response) => {
                if (response == Gtk.ResponseType.OK)
                {
                    config["saving-recordings-save-dir"] = new GLib.Variant.string(saving_recordings_save_dir_button.label);
                    config["saving-pictures-save-dir"] = new GLib.Variant.string(saving_pictures_save_dir_button.label);
                    config["saving-output-filename-format"] = new GLib.Variant.string(saving_output_filename_format_entry.buffer.text);
                }
                else
                    settings_dialog.destroy();
            });

            settings_dialog.show();
        }

        // Validates settings dialog state.
        private bool validate_settings_dialog_state(Gtk.Builder builder_settings_dialog)
        {
            var saving_output_filename_format_entry = (Gtk.Entry) builder_settings_dialog.get_object("saving-output-filename-format-entry");

            return saving_output_filename_format_entry.buffer.text.length != 0;
        }

        // Called when the user attempts to close a window and terminates the application immediately
        // if recording is not in progress, otherwise confirmation dialog is displayed.
        private bool window_close(Gtk.Window window)
        {
            if (pipeline.is_recording)
            {
                var builder_recording_in_progress_quit_dialog = new Gtk.Builder.from_string(res_recording_in_progress_quit_dialog, -1);
                var recording_in_progress_quit_dialog = (Gtk.MessageDialog) builder_recording_in_progress_quit_dialog.get_object("recording-in-progress-quit-dialog");

                recording_in_progress_quit_dialog.transient_for = window;

                var icon = new Gtk.Image.from_icon_name("dialog-warning-symbolic");
                icon.icon_size = Gtk.IconSize.LARGE;
                recording_in_progress_quit_dialog.get_content_area().prepend(icon);

                recording_in_progress_quit_dialog.response.connect((response) => {
                    recording_in_progress_quit_dialog.destroy();

                    if (response == Gtk.ResponseType.YES)
                    {
                        pipeline.stop_recording();
                        quit();
                    }
                });

                recording_in_progress_quit_dialog.show();
            }
            else
                quit();

            return true;
        }

        // Sets up internationalization.
        private static void i18n(string locale_dir, string text_domain, string codeset = "UTF-8")
        {
            try
            {
                string _locale_dir = GLib.Filename.from_utf8(locale_dir, -1, null, null);
                string _text_domain = GLib.Filename.from_utf8(text_domain, -1, null, null);
                string _codeset = GLib.Filename.from_utf8(codeset, -1, null, null);

                GLib.Intl.bindtextdomain(_text_domain, _locale_dir);
                GLib.Intl.bind_textdomain_codeset(_text_domain, _codeset);
                GLib.Intl.textdomain(_text_domain);
            }
            catch (GLib.ConvertError error)
            {
                stderr.printf(GLib._("Failed to setup internationalization: "));
                stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
            }
        }

        public static int main(string[] args)
        {
            // Setup internationalization.
            i18n(Symbols.LOCALE_DIR, Symbols.EXEC_NAME);

            // Set user-friendly application name.
            GLib.Environment.set_application_name(Symbols.TITLE);

            // Initialize GStreamer.
            Gst.init(ref args);

            // Create and run the application.
            // Return its exit code.
            return new Application(Symbols.IDENTIFIER).run(args);
        }
    }
}
