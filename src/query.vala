/*
 * Copyright (C) 2021 Nikola Hadžić
 *
 * This file is part of Basirec.
 *
 * Basirec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Basirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Basirec.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Basirec
{
    namespace Query
    {
        // Collects all cameras into a hash map and returns it.
        // The hash map maps unique camera identifiers to their
        // corresponding user-readable names.
        Gee.HashMap<string, string> find_cameras()
        {
            var dev_video = new Gee.HashMap<string, GLib.Variant>();
            var dev_media = new Gee.HashMap<string, GLib.Variant>();

            try
            {
                var regex_is_video = new GLib.Regex("^video\\d+$", GLib.RegexCompileFlags.OPTIMIZE);
                var regex_is_media = new GLib.Regex("^media\\d+$", GLib.RegexCompileFlags.OPTIMIZE);

                string DEVICE_DIR = GLib.Filename.from_utf8("/dev", -1, null, null);
                var device_dir = GLib.Dir.open(DEVICE_DIR);

                for (string? device;(device = device_dir.read_name()) != null;)
                {
                    bool is_video = regex_is_video.match(device);
                    bool is_media = !is_video && regex_is_media.match(device);

                    if (is_video || is_media)
                    {
                        device = GLib.Path.build_filename(DEVICE_DIR, device);

                        try
                        {
                            for (string? link;(link = GLib.FileUtils.read_link(device)) != null;)
                                device = GLib.Filename.canonicalize(link, GLib.Path.get_dirname(device));   // NOTE: Links are resolved from their own directory.
                        }
                        catch (GLib.FileError error)
                        {
                            if (error.code != GLib.FileError.INVAL)
                                throw error;
                        }

                        int fd = Posix.open(device, Posix.O_RDWR);
                        if (fd == -1)
                        {
                            try
                            {
                                stderr.printf(GLib._("Failed to open device \"%s\".\n"), GLib.Filename.to_utf8(device, -1, null, null));
                            }
                            catch (GLib.ConvertError error)
                            {
                                stderr.printf(GLib._("Failed to open a device, path unknown.\n"));
                            }

                            continue;
                        }

                        if (is_video)
                        {
                            V4l2.Capability v4l2_caps;
                            if (Posix.ioctl(fd, V4l2.VIDIOC_QUERYCAP, out v4l2_caps) == 0 && (v4l2_caps.device_caps & V4l2.Capabilities.VIDEO_CAPTURE) != 0)
                                dev_video[v4l2_caps.bus_info] = new GLib.Variant("(ssb)", device, v4l2_caps.card, (v4l2_caps.device_caps & V4l2.Capabilities.IO_MC) != 0);
                        }
                        else if (is_media)
                        {
                            MediaController.DeviceInfo media_info;
                            if (Posix.ioctl(fd, MediaController.IOC.DEVICE_INFO, out media_info) == 0)
                                dev_media[media_info.bus_info] = new GLib.Variant("(ss)", device, media_info.model);
                        }

                        Posix.close(fd);
                    }
                }
            }
            catch (GLib.RegexError error)
            {
                stderr.printf(GLib._("Error querying cameras: "));
                stderr.printf(GLib._("Regex error: %s.\n"), error.message);
            }
            catch (GLib.ConvertError error)
            {
                stderr.printf(GLib._("Error querying cameras: "));
                stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
            }
            catch (GLib.FileError error)
            {
                stderr.printf(GLib._("Error querying cameras: "));
                stderr.printf(GLib._("File error: %s.\n"), error.message);
            }

            var cameras = new Gee.HashMap<string, string>();

            foreach (string bus_info in dev_media.keys)
            {
                if (dev_video.has_key(bus_info))
                {
                    var dev_video_iter = dev_video[bus_info].iterator();
                    string v4l2_device = dev_video_iter.next_value().get_string();
                    dev_video_iter.next_value();
                    bool v4l2_io_mc = dev_video_iter.next_value().get_boolean();

                    var dev_media_iter = dev_media[bus_info].iterator();
                    string media_device = dev_media_iter.next_value().get_string();
                    string media_model = dev_media_iter.next_value().get_string();

                    cameras[v4l2_io_mc ? media_device : v4l2_device] = media_model;

                    dev_video.unset(bus_info);
                }
            }

            foreach (var device in dev_video)
            {
                var iter = device.value.iterator();
                cameras[iter.next_value().get_string()] = iter.next_value().get_string();
            }

            return cameras;
        }

        // Collects all microphones into a hash map and returns it.
        // The hash map maps unique microphone identifiers to their
        // corresponding user-readable names.
        Gee.HashMap<string, string> find_microphones(
#if BASIREC_AUDIO_SYSTEM_PULSE
            string? pulseaudio_server = null
#endif
        )
        {
            var microphones = new Gee.HashMap<string, string>();

#if BASIREC_AUDIO_SYSTEM_ALSA
            unowned Alsa.CardInfo card_info;
            Alsa.CardInfo.alloca(out card_info);

            unowned Alsa.PcmInfo pcm_info;
            Alsa.PcmInfo.alloca(out pcm_info);

            for (int card_number = -1;;)
            {
                if (Alsa.Card.next(ref card_number) < 0)
                {
                    stderr.printf(GLib._("Error while getting sound card number %d.\n"), card_number + 1);
                    break;
                }

                if (card_number == -1)
                    break;

                string card_name = "hw:" + card_number.to_string();

                Alsa.Card card;
                if (Alsa.Card.open(out card, card_name) < 0)
                {
                    stderr.printf(GLib._("Failed opening sound card \"%s\".\n"), card_name);
                    continue;
                }

                if (card.card_info(card_info) < 0)
                {
                    stderr.printf(GLib._("Failed to get information from sound card \"%s\".\n"), card_name);
                    continue;
                }

                for (int device_number = -1;;)
                {
                    if (card.pcm_next_device(ref device_number) < 0)
                    {
                        stderr.printf(GLib._("Error while getting device %d from sound card \"%s\".\n"), device_number + 1, card_name);
                        continue;
                    }

                    if (device_number < 0)
                        break;

                    pcm_info.set_device(device_number);
                    pcm_info.set_subdevice(0);
                    pcm_info.set_stream(Alsa.PcmStream.CAPTURE);

                    string device_name = card_name + "," + device_number.to_string();

                    {
                        int ret = card.pcm_info(pcm_info);
                        if (ret < 0)
                        {
                            if (ret != -Posix.ENOENT)
                                stderr.printf(GLib._("Error while querying sound card device \"%s\".\n"), device_name);

                            continue;
                        }
                    }

                    microphones[device_name] = card_info.get_name() + " - " + pcm_info.get_name();
                }
            }

            Alsa.Config.Update.free_global(); 
#elif BASIREC_AUDIO_SYSTEM_PULSE
            PulseAudio.MainLoop mainloop = new PulseAudio.MainLoop(); 
            PulseAudio.MainLoopApi mainloop_api = mainloop.get_api();
            PulseAudio.Context context = new PulseAudio.Context(mainloop_api, Symbols.EXEC_NAME);

            context.set_state_callback((context) => {
                switch (context.get_state())
                {
                    case PulseAudio.Context.State.READY:
                        PulseAudio.Operation? operation = context.get_source_info_list((context, info, eol) => {
                            if (eol < 0)
                            {
                                stderr.printf(GLib._("Failed to get audio source \"%s\" information: %s.\n"), info.name, PulseAudio.strerror(context.errno()));
                                mainloop_api.quit(mainloop_api, 1);
                            } else if (eol > 0)
                                context.disconnect();
                            else if (info.monitor_of_sink_name == null)
                                microphones[info.name] = info.proplist.gets(PulseAudio.Proplist.PROP_DEVICE_API) == "alsa" ? info.proplist.gets("alsa.card_name") + " - " + info.proplist.gets("alsa.name") : info.proplist.gets(PulseAudio.Proplist.PROP_DEVICE_PRODUCT_NAME);
                        });

                        if (operation == null)
                        {
                            stderr.printf(GLib._("Listing audio sources failed: %s.\n"), PulseAudio.strerror(context.errno()));
                            context.disconnect();
                        }
                        break;
                    case PulseAudio.Context.State.TERMINATED:
                        mainloop_api.quit(mainloop_api, 0);
                        break;
                    case PulseAudio.Context.State.FAILED:
                        stderr.printf(GLib._("Could not connect to PulseAudio server: %s.\n"), PulseAudio.strerror(context.errno()));
                        mainloop_api.quit(mainloop_api, 1);
                        break;
                    default:
                        break;
                }
            });

            if (context.connect(pulseaudio_server, PulseAudio.Context.Flags.NOAUTOSPAWN) < 0)
                stderr.printf(GLib._("Could not connect to PulseAudio server: %s.\n"), PulseAudio.strerror(context.errno()));
            else if (mainloop.run() < 0)
                stderr.printf(GLib._("Running PulseAudio loop failed.\n"));
#endif

            return microphones;
        }
    }
}
