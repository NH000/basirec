/*
 * Copyright (C) 2021-2022 Nikola Hadžić
 *
 * This file is part of Basirec.
 *
 * Basirec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Basirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Basirec.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Basirec
{
    // Errors that could occur in the pipeline.
    errordomain PipelineError
    {
        ELEMENT_CREATE_FAILURE,
        ELEMENT_LINK_FAILURE,
        ELEMENT_RESOURCE_BUSY
    }

    class Pipeline : Gst.Pipeline
    {
        // Current operation of the pipeline.
        public bool is_recording { get; private set; default = false; }
        public bool is_recording_paused { get; private set; default = false; }

        // Pipeline elements.
        private Gst.Element video_src;
        private Gst.Element video_convert;
        private Gst.Element preview_sink;

        public Pipeline() throws PipelineError
        {

            Object();

            var bus = get_bus();
            bus.add_watch(GLib.Priority.DEFAULT, on_bus_message);

            video_src = Gst.ElementFactory.make("v4l2src", "video-src");
            video_convert = Gst.ElementFactory.make("videoconvert", "video-convert");
            preview_sink = Gst.ElementFactory.make("gtk4sink", "video-preview");

            if (video_src == null || video_convert == null || preview_sink == null)
                throw new PipelineError.ELEMENT_CREATE_FAILURE(GLib._("Not all elements could be created"));

            add_many(video_src, video_convert, preview_sink);
            if (!video_src.link_many(video_convert, preview_sink))
                throw new PipelineError.ELEMENT_LINK_FAILURE(GLib._("Not all elements could be linked"));
        }

        // Shuts down the pipeline and removes bus watchers.
        ~Pipeline()
        {
            var bus = get_bus();
            bus.remove_watch();

            set_state(Gst.State.NULL);
        }

        // Returns reference to the camera preview widget.
        public unowned Gtk.Widget get_camera_preview()
        {
            unowned Gtk.Widget camera_preview;
            preview_sink.get("widget", out camera_preview);
            return camera_preview;
        }

        // Listens for and acts on bus messages.
        private bool on_bus_message(Gst.Bus bus, Gst.Message message)
        {
            GLib.Error error;

            switch (message.type)
            {
#if BASIREC_PIPELINE_WARNINGS
                case Gst.MessageType.WARNING:
                    message.parse_warning(out error, null);
                    stderr.printf(GLib._("Pipeline warning: element %s: %s\n"), message.src.get_name(), error.message);
                    break;
#endif
                case Gst.MessageType.ERROR:
                    message.parse_error(out error, null);
                    stderr.printf(GLib._("Pipeline error: element %s: %s\n"), message.src.get_name(), error.message);
                    break;
                default:
                    break;
            }

            return true;
        }

        // Sets the supplied camera as the video source and starts up the pipeline.
        // If the pipeline is already active it is first shut down.
        // You must not call this function while the pipeline is recording.
        public void set_video_source(string camera) throws PipelineError
            requires(!is_recording)
        {
            set_state(Gst.State.NULL);

            video_src.set("device", camera);
            set_state(Gst.State.PLAYING);

            if (get_state(null, null, Gst.CLOCK_TIME_NONE) == Gst.StateChangeReturn.FAILURE)
            {
                set_state(Gst.State.NULL);
                throw new PipelineError.ELEMENT_RESOURCE_BUSY(GLib._("Camera resource busy"));
            }
        }

        // Starts the recording process.
        // You must not call this function while the pipeline is recording.
        public void start_recording(string? microphone, string output_filename)
            requires(!is_recording)
        {
            // TODO: Start recording.

            is_recording = true;
        }

        // Stops the recording process.
        // You must call this function while the pipeline is recording.
        public void stop_recording()
            requires(is_recording)
        {
            // TODO: Stop the recording.

            is_recording_paused = false;
            is_recording = false;
        }

        // Pauses the recording process.
        // You must call this function while the pipeline is recording and not paused.
        public void pause_recording()
            requires(is_recording && !is_recording_paused)
        {
            // TODO: Pause the recording.

            is_recording_paused = true;
        }

        // Pauses the recording process.
        // You must call this function while the pipeline is recording and paused.
        public void resume_recording()
            requires(is_recording && is_recording_paused)
        {
            // TODO: Resume the recording.

            is_recording_paused = false;
        }

        // Takes and saves a picture.
        // You must not call this function while the pipeline is recording.
        public void take_a_picture(string output_filename)
            requires(!is_recording)
        {
            // TODO: Take a picture.
        }
    }
}
