/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of Basirec.
 *
 * Basirec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Basirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Basirec.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Basirec
{
    // Displays file chooser to the user when the button is clicked,
    // and when the user selects a file, button's label is set to the of the chosen file.
    public class FileChooserButton : Gtk.Button
    {
        // Properties for the dialog.
        public string? dialog_title { get; set; default = null; }
        public Gtk.Window? dialog_transient_for { get; set; default = null; }
        public bool dialog_modal { get; set; default = false; }
        public Gtk.FileChooserAction dialog_action { get; set; default = Gtk.FileChooserAction.SELECT_FOLDER; }
        public string dialog_positive_action_label { get; set; default = GLib._("Select"); }
        public bool dialog_create_folders { get; set; default = true; }

        // Is run when the user selects a file.
        // Its argument is selected file in UTF-8 encoding.
        public virtual signal void selected(GLib.File path);

        public FileChooserButton(string? dialog_title, Gtk.Window? dialog_transient_for, bool dialog_modal, Gtk.FileChooserAction dialog_action)
        {
            Object(dialog_title: dialog_title, dialog_transient_for: dialog_transient_for, dialog_modal: dialog_modal, dialog_action: dialog_action);
        }

        // Displays file chooser dialog which lets the user pick a file.
        public override void clicked()
        {
            var file_chooser_dialog = new Gtk.FileChooserDialog(dialog_title, dialog_transient_for, dialog_action, GLib._("Cancel"), Gtk.ResponseType.CANCEL, dialog_positive_action_label, Gtk.ResponseType.OK);
            file_chooser_dialog.modal = dialog_modal;
            file_chooser_dialog.create_folders = dialog_create_folders;

            if (label != null && label.length != 0)
                try { file_chooser_dialog.set_file(GLib.File.new_for_path(label)); } catch (GLib.Error error) { } // NOTE: Exception here will never be thrown.

            file_chooser_dialog.response.connect((response) => {
                if (response == Gtk.ResponseType.OK)
                {
                    var chosen_file = file_chooser_dialog.get_file();

                    try
                    {
                        label = GLib.Filename.to_utf8(chosen_file.peek_path(), -1, null, null);
                        selected(chosen_file);
                    }
                    catch (GLib.ConvertError error)
                    {
                        stderr.printf(GLib._("Failed to set file chooser button label to the path of the selected file: "));
                        stderr.printf(GLib._("Filename conversion error: %s.\n"), error.message);
                    }
                }

                file_chooser_dialog.destroy();
            });

            file_chooser_dialog.show();
        }
    }
}
