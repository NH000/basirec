/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of Basirec.
 *
 * Basirec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Basirec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Basirec.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Basirec
{
    extern unowned GLib.Resource get_resource();
}
