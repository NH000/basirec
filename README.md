# Basirec

## Description
GUI recorder for camera and microphone. Available features are pretty standard, and include:

+ Selection of the recording devices
+ Live camera preview
+ ~~Video and audio effects~~ (not yet)
+ Configuration of the saving format (encoder and container, including those for static images)
+ Support for multiple audio systems

## Requirements
+ meson
+ make
+ valac
+ cc
+ x11 (only if graphics backend is X11)
+ alsa / libpulse
+ gee-0.8
+ gtk4
+ gstreamer-1.0
+ gst-plugins-base (only if audio system to use is ALSA)
+ gst-plugins-good
+ gettext
+ msgfmt

GCC and Clang are guaranteed to compile the project.

## Installation
Here are the steps for the basic installation, make sure that you are in the project's root directory:

```
meson setup -Dbuildtype=release build
meson compile -C build
meson install -C build
make -C po install
```

You can change build options if you do not like the defaults, run `meson configure -C build`
(after the first command) to see them. The same is true for the makefile in the PO directory.

## Uninstallation
For the uninstallation you will need `make` and `sh`.

Just go into the project's root directory and run:

```
make -f uninstall.mk uninstall
```

Make sure that all make variables are set to the values used during the installation (relevant meson options are duplicated in the makefile),
run `make -f uninstall.mk help` to see them.

Uninstallation process will not remove the configuration file.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions** |
|--------------------|----------------------------------------------------------|------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |

### Translation process
This program is written to be easily translatable into multiple languages, and it achieves that through the use of [`gettext`](https://www.gnu.org/software/gettext/) library.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

You must be in `po` directory when running the subsequent commands.

To add/update a translation, you must first generate POT file:

```
make messages.pot
```

To create/update PO files run `make language-code.po`. The command will both update already existing PO files and create PO files for newly added languages.
Then you can edit them (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details), translating the program that way.

In PO files you should find untranslated line `msgid "translator-credits"`: as translation write your own name (with optionally your email/website) there.

Manual page should also be translated, you will find the original one in `man` directory and there you should add translations too, with filename `basirec.language-code.1`.
This will also require update of the build script to configure and install the added pages.

Also, it is recommended to add/update translations of `.desktop` file entry as well, its template is `basirec.desktop.in`.
